# Inferred Gender

Creating a superior inferred gender table. DBT logic lives [here](https://git.treatwell.net/analytics/transformer/-/merge_requests/24/diffs?commit_id=b507783a2addaca5ca2bdbad66164ce8e1ffd0ae)


A first name to gender dictionary is created at `marketing_scratch.inf_gender`. This maps first names along to gender proportions of all users. Multiple data sources are used to create this table, including private APIs and public US social security data.

The first names of our users are then joined to this table to create a prediction for every single user. The logic for our best guess at gender is:

1. Self-reported gender
2. Inferred gender based on first name.


## To do

Add an intermediate step using `public.content_preference` field, which is also self report.
